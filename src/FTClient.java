public class FTClient {

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("Usage: java FTClient [file_name] [DNS name/IP address] [port_number]");
            return;
        }
        Client client = new Client(args[0], args[1], args[2]);
        client.start();
    }
}
